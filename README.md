# MeadHead_API_Services

## Architecture du systeme/application -------------

Stack technologique:

- Spring Initializer
- SpringBoot
- Java
- Maven
- PostgreSQL
- GitLab
- GitLab CI/CD

- 5 couches:
	- Controller:
	- Entity:
	- Exception:
	- Repository:
	- Service:
	
## Dependencies ______________________

- spring-boot-starter-data-jpa
- spring-boot-starter-test
- spring-boot-maven-plugin
- lombok
- postgresql

## Properties ______________________	

- Java 18
- Maven-apache

## Main ______________________
	
- MedHeadApplication


## Interfaces ______________________

- HospitalRepository
- PatientRepository
- SpecialtyRepository
- WorkForceRepository



## Name
MedHead API Services

## Description
This repository contains the POC application for MedHead API Services


